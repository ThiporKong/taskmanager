## Design

`Process` encapsulates the design's external contract. It abstracts over any potential representation of priorities.

Internally, the design is centered around the `TaskManager` interface, which is abstracted over any specific
implementation of `Process`.

Concerns are separated as decorators, which can be composed by delegation:

- Effects: `TaskManager.killing()`
- Concurrency: `TaskManager.synchronized()`
- Duplicate prevention: `TaskManager.noDuplicates()`
- Capacity management: `TaskManagerQueue.bounded()`
- Backing data structure: `TaskManagerAbstract` and `TaskManagerQueue`

`policies.kt` implements the required three policies.

## Performance

Performance is achieved by

- using proper data structures: linked list for FIFO and double-ended priority queue for the priority-based policy
- implementing duplicate prevention with a hash-set of processes separate from the tasks collection 

## Quality

- `TaskManagerTest` defines property-based tests for the three policies
- `sonarqube`, `jacoco`, and `detekt` are configured
- Test coverage is 99%
