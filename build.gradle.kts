import io.gitlab.arturbosch.detekt.Detekt
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.32" // "1.5.10" - jacoco fails for 1.5
    jacoco
    id("io.gitlab.arturbosch.detekt").version("1.17.1")
    id("org.sonarqube") version "3.2.0"
    id("org.jetbrains.dokka") version "1.4.32"
}

val junitVersion = "5.7.0"
val jacocoReportPath = buildDir.resolve("jacoco/jacocoTestReport.xml")
val jvmTargetVersion = "1.8"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib")
    implementation("com.google.guava:guava:30.1.1-jre")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
    testImplementation("net.jqwik:jqwik:1.5.1")
    testImplementation("org.assertj:assertj-core:3.12.2")
}

tasks {
    test {
        useJUnitPlatform {
            includeEngines("jqwik")
        }
        finalizedBy(jacocoTestReport)
    }

    jacocoTestReport {
        reports {
            xml.isEnabled = true
            xml.destination = jacocoReportPath
        }
    }

    withType<KotlinCompile>().configureEach {
        kotlinOptions.jvmTarget = jvmTargetVersion
    }

    withType<Detekt>().configureEach {
        jvmTarget = jvmTargetVersion
    }
}

sonarqube {
    properties {
        property("sonar.coverage.jacoco.xmlReportPaths", jacocoReportPath)
    }
}
