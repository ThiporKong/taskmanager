package taskmanager

import net.jqwik.api.stateful.Action
import org.assertj.core.api.Assertions.assertThat
import taskmanager.TaskManagerTest.Priority
import taskmanager.TaskManagerTest.Process

internal data class ActionAdd2(val priority: Priority) : Action<TaskManager<Process>> {
    override fun run(state: TaskManager<Process>): TaskManager<Process> {
        val process = Process(priority)
        val before = state.list()
        val evicted = state.add(process)
        val after = state.list()
        assertThat(after).last().matches { it.process == process }
        if (before.size < TaskManagerTest.capacity) {
            assertThat(evicted).isEmpty()
            assertThat(after.size).isEqualTo(before.size + 1)
        } else {
            assertThat(evicted).hasSize(1).first().isEqualTo(before.first())
            assertThat(after.size).isEqualTo(before.size)
        }
        return state
    }
}
