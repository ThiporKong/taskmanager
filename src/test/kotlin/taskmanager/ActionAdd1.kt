package taskmanager

import net.jqwik.api.stateful.Action
import org.assertj.core.api.Assertions.assertThat
import taskmanager.TaskManagerTest.Priority
import taskmanager.TaskManagerTest.Process

internal data class ActionAdd1(val priority: Priority) : Action<TaskManager<Process>> {
    override fun run(state: TaskManager<Process>): TaskManager<Process> {
        val process = Process(priority)
        val before = state.list()
        if (before.size < TaskManagerTest.capacity) {
            assertThat(state.add(process)).isEmpty()
            assertThat(state.list())
                .containsAll(before)
                .last().matches { it.process == process }
        } else {
            assertThat(state.add(process)).isNull()
        }
        return state
    }
}
