package taskmanager

import net.jqwik.api.stateful.Action
import org.assertj.core.api.Assertions.assertThat
import taskmanager.TaskManagerTest.Process

internal data class ActionRemove(val indices: Set<Int>) : Action<TaskManager<Process>> {
    override fun run(state: TaskManager<Process>): TaskManager<Process> {
        val before = state.list()
        if (before.isNotEmpty()) {
            val toRemove = indices.map { before[it % before.size] }.map { it.process }.toSet()
            val removed = state.remove { toRemove.contains(it.process) }
            if (toRemove.isNotEmpty()) {
                assertThat(state.list().map { it.process }.toSet()).doesNotContainAnyElementsOf(toRemove)
            }
            assertThat(removed.map { it.process }.toSet()).isEqualTo(toRemove)
        }
        return state
    }
}
