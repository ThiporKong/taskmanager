package taskmanager

import net.jqwik.api.stateful.Action
import org.assertj.core.api.Assertions.assertThat
import taskmanager.TaskManagerTest.Priority
import taskmanager.TaskManagerTest.Process

internal data class ActionAdd3(val priority: Priority) : Action<TaskManager<Process>> {
    override fun run(state: TaskManager<Process>): TaskManager<Process> {
        val process = Process(priority)
        val before = state.list()
        val evicted = state.add(process)
        if (before.size < TaskManagerTest.capacity) {
            assertThat(evicted).isEmpty()
            assertThat(state.list()).anyMatch { it.process == process }
        } else {
            if (before.maxOf { it.process.priority } < priority) {
                assertThat(evicted).hasSize(1).first().isEqualTo(before.first())
                assertThat(state.list()).anyMatch { it.process == process }
            } else {
                assertThat(evicted).isNull()
            }
        }
        return state
    }
}
