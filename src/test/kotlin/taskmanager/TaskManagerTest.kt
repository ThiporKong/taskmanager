package taskmanager

import net.jqwik.api.Arbitraries.*
import net.jqwik.api.ForAll
import net.jqwik.api.Property
import net.jqwik.api.Provide
import net.jqwik.api.constraints.IntRange
import net.jqwik.api.stateful.Action
import net.jqwik.api.stateful.ActionSequence
import org.junit.jupiter.api.assertThrows
import java.util.concurrent.atomic.AtomicInteger
import kotlin.Int.Companion.MIN_VALUE

internal object TaskManagerTest {

    enum class Priority { LOW, MEDIUM, HIGH }

    data class Process(
        override val priority: Priority,
        @Suppress("UnusedPrivateMember") val pid: Int = pids.getAndIncrement()
    ) : taskmanager.Process<Priority> {
        companion object {
            private val pids = AtomicInteger()
        }
    }

    const val capacity = 5

    @Property
    fun testInvalidCapacity(@ForAll @IntRange(min = MIN_VALUE, max = 0) invalidCapacity: Int) {
        assertThrows<IllegalStateException> { policy1<Process>(invalidCapacity) }
    }

    @Property
    fun testPolicy1(@ForAll("actions1") actions: ActionSequence<TaskManager<Process>>) {
        actions.run(policy1(capacity))
    }

    @Property
    fun testPolicy2(@ForAll("actions2") actions: ActionSequence<TaskManager<Process>>) {
        actions.run(policy2(capacity))
    }

    @Property
    fun testPolicy3(@ForAll("actions3") actions: ActionSequence<TaskManager<Process>>) {
        actions.run(policy3(capacity))
    }

    @Provide
    fun actions1() = sequences(oneOf(addAction(::ActionAdd1), addExistingAction(), removeAction()))

    @Provide
    fun actions2() = sequences(oneOf(addAction(::ActionAdd2), addExistingAction(), removeAction()))

    @Provide
    fun actions3() = sequences(oneOf(addAction(::ActionAdd3), addExistingAction(), removeAction()))

    private fun <V, A : Action<V>> addAction(factory: (Priority) -> A) = of(Priority::class.java).map(factory)

    private fun addExistingAction() = integers().between(0, capacity).map(::ActionAddExisting)

    private fun removeAction() = integers().between(0, capacity).set().map(::ActionRemove)
}
