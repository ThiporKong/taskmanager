package taskmanager

import net.jqwik.api.stateful.Action
import org.assertj.core.api.Assertions.assertThat
import taskmanager.TaskManagerTest.Process

internal data class ActionAddExisting(val index: Int) : Action<TaskManager<Process>> {
    override fun run(state: TaskManager<Process>): TaskManager<Process> {
        val before = state.list()
        if (before.isNotEmpty()) {
            val existing = before[index % before.size]
            assertThat(state.add(existing.process)).isNull()
        }
        return state
    }
}
