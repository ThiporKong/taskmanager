package taskmanager

import taskmanager.TaskManagerQueue.Companion.linkedList
import taskmanager.TaskManagerQueue.Companion.minMaxPriorityQueue
import java.util.Comparator.comparing

/** Default */
fun <PROC : Process<*>> policy1(capacity: Int) =
    linkedList<PROC>()
        .bounded(capacity) { _, _ -> false }
        .noDuplicates()
        .synchronized()
        .killing()

/** FIFO */
fun <PROC : Process<*>> policy2(capacity: Int) =
    linkedList<PROC>()
        .bounded(capacity) { _, _ -> true }
        .noDuplicates()
        .synchronized()
        .killing()

/** Priority-based - use min-max priority queue for efficient access to head and tail */
@Suppress("UnstableApiUsage")
fun <PROC : Process<PRIO>, PRIO : Comparable<PRIO>> policy3(capacity: Int) =
    minMaxPriorityQueue(comparing<Task<PROC>, PRIO> { it.process.priority }.thenComparingLong { it.started })
        .bounded(capacity) { queue, process -> queue.peekLast().process.priority < process.priority }
        .noDuplicates()
        .synchronized()
        .killing()
