package taskmanager

/**
 * [Task] represents a [Process] managed by [TaskManager].
 *
 * @param PROC Specific implementation of [Process]
 */
data class Task<PROC : Process<*>>(val process: PROC, val started: Long)

