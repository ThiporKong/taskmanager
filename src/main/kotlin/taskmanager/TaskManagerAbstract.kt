package taskmanager

import java.util.concurrent.atomic.AtomicLong
import java.util.function.Predicate

/**
 * Abstract [TaskManager] backed by [MutableCollection].
 */
abstract class TaskManagerAbstract<PROC : Process<*>> : TaskManager<PROC> {
    protected abstract val tasks: MutableCollection<Task<PROC>>

    private val time = AtomicLong()

    override fun list() = ArrayList(tasks)

    override fun add(process: PROC): List<Task<PROC>>? {
        tasks.add(Task(process, time.getAndIncrement()))
        return listOf()
    }

    override fun remove(predicate: Predicate<Task<PROC>>): List<Task<PROC>> {
        val matches = tasks.filter(predicate::test)
        tasks.removeAll(matches)
        return matches
    }
}
