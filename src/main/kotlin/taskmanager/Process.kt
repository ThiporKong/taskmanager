package taskmanager

/**
 * [Process] describes the contract for processes as expected by [TaskManager].
 *
 * [Process]es are identified using regular object identity.
 * Hence PID is not explicitly modelled, but one is free to do so when implementing [Process].
 *
 * @param PRIO Specific type for representing priorities
 */
interface Process<PRIO> {

    val priority: PRIO

    /**
     * [kill] is the sole possible effect on a [Process].
     *
     * It can be triggered by [TaskManager] after eviction or removal of [Task]s.
     * From the perspective of this design, success or failure of [kill]
     * is encapsulated in the implementation of Process.
     */
    fun kill() = Unit
}
