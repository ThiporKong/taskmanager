package taskmanager

import com.google.common.collect.MinMaxPriorityQueue
import taskmanager.TaskManagerQueue.Companion.linkedList
import taskmanager.TaskManagerQueue.Companion.minMaxPriorityQueue
import java.util.*
import java.util.function.Predicate

/**
 * [TaskManager] backed by [Queue].
 * [linkedList] and [minMaxPriorityQueue] provide common queue implementations.
 *
 * [TaskManagerQueue] can be augmented with capacity management using [bounded]:
 * If capacity is reached, a policy specifies whether to evict the queue head for making space.
 *
 * @param PROC Specific implementation of [Process]
 * @param COLL Specific type for backing collection
 */
open class TaskManagerQueue<PROC : Process<*>, COLL : Queue<Task<PROC>>>(override val tasks: COLL) :
    TaskManagerAbstract<PROC>() {
    /**
     * Augments [TaskManager] with capacity management.
     * @param capacity Maximum of [Task]s
     * @param policy Decide whether to make space by evicting queue head
     * @return [TaskManager] with capacity management
     */
    fun bounded(capacity: Int, policy: (COLL, PROC) -> Boolean) =
        object : TaskManager<PROC> {
            init {
                check(capacity > 0) { "capacity=$capacity must be positive" }
            }

            override fun list() = this@TaskManagerQueue.list()
            override fun remove(predicate: Predicate<Task<PROC>>) = this@TaskManagerQueue.remove(predicate)

            override fun add(process: PROC) =
                with(this@TaskManagerQueue) {
                    val evicted = evictConditionally(process)
                    if (tasks.size < capacity) {
                        add(process)
                        evicted
                    } else {
                        null
                    }
                }

            private fun evictConditionally(process: PROC): List<Task<PROC>> =
                if (tasks.size >= capacity && policy.invoke(tasks, process)) {
                    listOf(tasks.remove())
                } else {
                    listOf()
                }
        }

    companion object {
        /** Create [TaskManagerQueue] backed by [LinkedList]. */
        fun <PROC : Process<*>> linkedList() =
            TaskManagerQueue<PROC, LinkedList<Task<PROC>>>(LinkedList())

        /**
         * Create [TaskManagerQueue] backed by [PriorityQueue].
         * @param comparator ordering for priority queue
         */
        @Suppress("UnstableApiUsage")
        fun <PROC : Process<*>> minMaxPriorityQueue(comparator: Comparator<Task<PROC>>) =
            TaskManagerQueue(MinMaxPriorityQueue.orderedBy(comparator).create())
    }
}
