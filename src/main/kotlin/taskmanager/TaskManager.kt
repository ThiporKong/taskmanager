package taskmanager

import java.util.function.Predicate

/**
 * [TaskManager] manages [Process]es as (prioritized) [Task]s.
 *
 * [synchronized] and [killing]
 * augment a [TaskManager] with the respective capabilities.
 * The order of applying [synchronized] and [killing] is significant,
 * as killing happens either inside or outside of synchronization boundaries.
 *
 * @param PROC Specific implementation of [Process]
 */
interface TaskManager<PROC : Process<*>> {
    /** Lists [Task]s in the [TaskManager]. Order is determined by backing data structure. */
    fun list(): List<Task<PROC>>

    /**
     * Adds a [Process] to [TaskManager].
     * @return null if process already present or space insufficient, otherwise (possibly empty) list of evicted [Task]s
     */
    fun add(process: PROC): List<Task<PROC>>?

    /**
     * Remove the [Task]s matching the predicate.
     * @return List of removed [Task]s
     */
    fun remove(predicate: Predicate<Task<PROC>>): List<Task<PROC>>

    /** Augments [TaskManager] with thread-safe synchronization. */
    fun synchronized(): TaskManager<PROC> =
        object : TaskManager<PROC> {
            @Synchronized
            override fun list() = this@TaskManager.list()

            @Synchronized
            override fun add(process: PROC) = this@TaskManager.add(process)

            @Synchronized
            override fun remove(predicate: Predicate<Task<PROC>>) = this@TaskManager.remove(predicate)
        }

    /**
     * Augments [TaskManager] to invoke [Process.kill] for evicted or removed [Task]s.
     */
    fun killing(): TaskManager<PROC> =
        object : TaskManager<PROC> {
            override fun list() = this@TaskManager.list()
            override fun add(process: PROC) = this@TaskManager.add(process)?.kill()
            override fun remove(predicate: Predicate<Task<PROC>>) = this@TaskManager.remove(predicate).kill()

            private fun <PROC : Process<*>> List<Task<PROC>>.kill(): List<Task<PROC>> {
                forEach { it.process.kill() }
                return this
            }
        }

    /**
     * Augments [TaskManager] with efficient duplicate protection.
     */
    fun noDuplicates(): TaskManager<PROC> =
        object : TaskManager<PROC> {
            private val processes = mutableSetOf<PROC>()

            override fun list() = this@TaskManager.list()

            override fun add(process: PROC) =
                if (processes.contains(process))
                    null
                else {
                    val evicted = this@TaskManager.add(process)
                    if (evicted != null) {
                        processes.add(process)
                        evicted.update()
                    } else {
                        null
                    }
                }

            override fun remove(predicate: Predicate<Task<PROC>>) = this@TaskManager.remove(predicate).update()

            private fun <PROC : Process<*>> List<Task<PROC>>.update(): List<Task<PROC>> {
                @Suppress("TYPE_INFERENCE_ONLY_INPUT_TYPES_WARNING")
                forEach { processes.remove(it.process) }
                return this
            }
        }
}
